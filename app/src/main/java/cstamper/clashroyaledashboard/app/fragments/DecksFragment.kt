package cstamper.clashroyaledashboard.app.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cstamper.clashroyaledashboard.R

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 22/08/2018.
 */
class DecksFragment: BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_decks, container, false)

        return view
    }
}