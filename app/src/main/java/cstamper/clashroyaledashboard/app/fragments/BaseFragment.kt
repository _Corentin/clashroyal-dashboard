package cstamper.clashroyaledashboard.app.fragments

import android.support.v4.app.Fragment

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 21/08/2018.
 */
open class BaseFragment: Fragment()