package cstamper.clashroyaledashboard.app.fragments.player

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cstamper.clashroyaledashboard.R
import cstamper.clashroyaledashboard.app.fragments.BaseFragment

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 23/08/2018.
 */
class EmptyPlayerFragment: BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_empty_player, container, false)

        return view
    }
}