package cstamper.clashroyaledashboard.app.activities

import android.os.Bundle
import android.support.design.widget.TabLayout
import cstamper.clashroyaledashboard.R
import cstamper.clashroyaledashboard.app.widgets.adapters.MainFragmentsAdapter
import kotlinx.android.synthetic.main.activity_main.*

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 21/08/2018.
 */
class MainActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = MainFragmentsAdapter(this@MainActivity, supportFragmentManager)
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.tabMode = TabLayout.MODE_FIXED

        for (i in 0 until tabLayout.tabCount) {
            val tab = tabLayout.getTabAt(i)
            tab?.customView = adapter.getTabView(i)
        }
    }
}