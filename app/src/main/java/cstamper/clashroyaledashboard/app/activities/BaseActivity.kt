package cstamper.clashroyaledashboard.app.activities

import android.support.v7.app.AppCompatActivity

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 21/08/2018.
 */
open class BaseActivity: AppCompatActivity()