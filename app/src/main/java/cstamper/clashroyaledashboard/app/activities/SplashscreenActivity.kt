package cstamper.clashroyaledashboard.app.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import cstamper.clashroyaledashboard.R
import cstamper.clashroyaledashboard.app.controllers.DI

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 17/08/2018.
 */
class SplashscreenActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)

        Handler().postDelayed(object: Runnable {
            override fun run() {
                startActivity(Intent(this@SplashscreenActivity, MainActivity::class.java))
            }
        }, 2000)
    }
}