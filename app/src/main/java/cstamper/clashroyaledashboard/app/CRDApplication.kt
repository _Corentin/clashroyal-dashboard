package cstamper.clashroyaledashboard.app

import android.app.Application
import cstamper.clashroyaledashboard.app.controllers.DI

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 17/08/2018.
 */
class CRDApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        // Initialize retrofit
        DI.INSTANCE.initialize(this)

        // Todo: Initialize database
        // Todo: Initialize fabric
    }
}