package cstamper.clashroyaledashboard.app.controllers

import cstamper.clashroyaledashboard.BuildConfig
import cstamper.clashroyaledashboard.app.CRDApplication
import cstamper.clashroyaledashboard.core.modules.application.datas.DatasModule

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
enum class DI {
    INSTANCE;

    private var datasModule: DatasModule? = null

    fun initialize(application: CRDApplication) {
        datasModule = DatasModule(
                application,
                BuildConfig.BASE_URL_API
        )
    }

    fun datas(): DatasModule {
        if (datasModule == null)
            throw IllegalStateException("DI.INSTANCE must be call initialize() on onCreate of CRDApplication.kt")
        return datasModule!!
    }
}