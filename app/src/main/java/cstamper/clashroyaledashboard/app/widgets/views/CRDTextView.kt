package cstamper.clashroyaledashboard.app.widgets.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 22/08/2018.
 */
class CRDTextView: TextView {

    constructor(context: Context, attrs: AttributeSet): super(context, attrs) {
        typeface = Typeface.createFromAsset(
                context.assets,
                "fonts/Clash-Regular.ttf")
    }
}