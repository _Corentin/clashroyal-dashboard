package cstamper.clashroyaledashboard.app.widgets.views

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.Button
import cstamper.clashroyaledashboard.R

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 24/08/2018.
 */
class CRDButton: Button {

    private var theme: Int = Theme.YELLOW

    constructor(context: Context, attrs: AttributeSet): super(context, attrs) {
        init(attrs)

        typeface = Typeface.createFromAsset(
                context.assets,
                "fonts/Clash-Regular.ttf")

        setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    val scaleDownX = ObjectAnimator.ofFloat(this, "scaleX", .9f)
                    val scaleDownY = ObjectAnimator.ofFloat(this, "scaleY", .9f)

                    scaleDownX.duration = 200
                    scaleDownY.duration = 200

                    val scaleDown = AnimatorSet()
                    scaleDown.play(scaleDownX).with(scaleDownY)
                    scaleDown.start()
                }
                MotionEvent.ACTION_UP -> {
                    val scaleDownX = ObjectAnimator.ofFloat(this, "scaleX", 1f)
                    val scaleDownY = ObjectAnimator.ofFloat(this, "scaleY", 1f)

                    scaleDownX.duration = 200
                    scaleDownY.duration = 200

                    val scaleDown = AnimatorSet()
                    scaleDown.play(scaleDownX).with(scaleDownY)
                    scaleDown.start()

                    performClick()
                }
            }
            false
        }
    }

    private fun init(attrs: AttributeSet) {
        context.obtainStyledAttributes(attrs, R.styleable.CRDButton, 0, 0).apply {
            try {
                theme = getInt(R.styleable.CRDButton_btnTheme, Theme.YELLOW)
                applyTheme()
            } finally {
                recycle()
            }
        }
    }

    private fun applyTheme() {
        when (theme) {
            Theme.YELLOW -> {
                setBackgroundResource(R.drawable.btn_yellow)
                setTextColor(resources.getColor(android.R.color.black))
            }

            Theme.BLUE -> {
                setBackgroundResource(R.drawable.btn_yellow)
                setTextColor(resources.getColor(android.R.color.black))
            }

            Theme.GREEN -> {
                setBackgroundResource(R.drawable.btn_yellow)
                setTextColor(resources.getColor(android.R.color.black))
            }

            Theme.RED -> {
                setBackgroundResource(R.drawable.btn_yellow)
                setTextColor(resources.getColor(android.R.color.black))
            }

            Theme.BLACK -> {
                setBackgroundResource(R.drawable.btn_yellow)
                setTextColor(resources.getColor(android.R.color.white))
            }
        }
    }

    class Theme {

        companion object {
            const val YELLOW = 0
            const val BLUE = 1
            const val GREEN = 2
            const val RED = 3
            const val BLACK = 4
        }
    }
}