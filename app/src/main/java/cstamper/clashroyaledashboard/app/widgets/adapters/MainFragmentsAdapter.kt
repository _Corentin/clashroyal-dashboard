package cstamper.clashroyaledashboard.app.widgets.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.View
import cstamper.clashroyaledashboard.R
import cstamper.clashroyaledashboard.app.fragments.*
import android.view.LayoutInflater
import cstamper.clashroyaledashboard.app.fragments.player.PlayerFragment
import kotlinx.android.synthetic.main.view_main_tab_item.view.*


/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 21/08/2018.
 */
class MainFragmentsAdapter(private val context: Context, fm: FragmentManager): FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return PlayerFragment()
            1 -> return ClanFragment()
            2 -> return BattlelogFragment()
            3 -> return DecksFragment()
            4 -> return SearchFragment()
        }

        return BaseFragment()
    }

    fun getTabView(position: Int): View? {
        val v = LayoutInflater.from(context).inflate(R.layout.view_main_tab_item, null)

        when (position) {
            0 -> {
                v.icon.setImageResource(R.drawable.ic_tab_0)
                v.title.setText(R.string.TAB_LAYOUT_TITLE_PLAYER)
                return v
            }
            1 -> {
                v.icon.setImageResource(R.drawable.ic_tab_1)
                v.title.setText(R.string.TAB_LAYOUT_TITLE_CLAN)
                return v
            }
            2 -> {
                v.icon.setImageResource(R.drawable.ic_tab_2)
                v.title.setText(R.string.TAB_LAYOUT_TITLE_BATTLELOG)
                return v
            }
            3 -> {
                v.icon.setImageResource(R.drawable.ic_tab_3)
                v.title.setText(R.string.TAB_LAYOUT_TITLE_DECKS)
                return v
            }
            4 -> {
                v.icon.setImageResource(R.drawable.ic_tab_4)
                v.title.setText(R.string.TAB_LAYOUT_TITLE_SEARCH)
                return v
            }
        }

        return null
    }

    override fun getCount(): Int {
        return 5
    }
}