package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
open class ResponseDTO {

    @SerializedName("reason")
    val reason: String? = null

    @SerializedName("message")
    val message: String? = null
}