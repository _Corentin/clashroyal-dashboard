package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 17/08/2018.
 */
class LeagueStatisticsDTO {

    @SerializedName("currentSeason")
    val currentSeason: LeagueSeasonDTO? = null

    @SerializedName("previousSeason")
    val previousSeason: LeagueSeasonDTO? = null

    @SerializedName("bestSeason")
    val bestSeason: LeagueSeasonDTO? = null

}