package cstamper.clashroyaledashboard.core.modules.retrofit

import android.content.Context
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import cstamper.clashroyaledashboard.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
open class RetrofitModule {

    private val retrofit: Retrofit

    constructor(context: Context, baseUrl: String): this(context, baseUrl, null)

    constructor(context: Context, baseUrl: String, interceptor: Interceptor?) {
        val builder = OkHttpClient.Builder()

        if (interceptor != null)
            builder.addInterceptor(interceptor)

        // Headers
        val headers = Interceptor { chain ->
            val builder = chain.request().newBuilder()
            builder.addHeader("Authorization", "Bearer " + BuildConfig.API_KEY)
            chain.proceed(builder.build())
        }
        builder.addInterceptor(headers)

        // Logs
        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
        }

        retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .client(builder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    fun retrofit(): Retrofit =
            retrofit
}