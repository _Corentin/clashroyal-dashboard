package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
class ChestDTO {

    @SerializedName("index")
    val index: Int? = null

    @SerializedName("name")
    val name: String? = null
}