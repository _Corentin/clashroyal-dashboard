package cstamper.clashroyaledashboard.core.modules.application.datas.resources

import cstamper.clashroyaledashboard.core.modules.application.models.dtos.CardDTO
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.ListResponseDTO
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
interface CardsEndpoint {

    @GET("v1/cards")
    fun cards(): Observable<ListResponseDTO<CardDTO>>
}