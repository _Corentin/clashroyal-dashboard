package cstamper.clashroyaledashboard.core.modules.application.models

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
class Constantes {

    companion object {
        const val PATH_CLAN_TAG = "clanTag"
        const val PATH_PLAYER_TAG = "playerTag"
        const val PATH_TOURNAMENT_TAG = "tournamentTag"
        const val PATH_LOCATION_ID = "locationId"

        const val QUERY_NAME = "name"
        const val QUERY_LOCATION_ID = "locationId"
        const val QUERY_MIN_MEMBERS = "minMembers"
        const val QUERY_MAX_MEMBERS = "maxMembers"
        const val QUERY_MIN_SCORE = "minScore"
        const val QUERY_LIMIT = "limit"
        const val QUERY_AFTER = "after"
        const val QUERY_BEFORE = "before"
    }
}