package cstamper.clashroyaledashboard.core.modules.application.datas.resources

import cstamper.clashroyaledashboard.core.modules.application.models.Constantes
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.BattleLogDTO
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.ChestDTO
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.ListResponseDTO
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.PlayerDTO
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
interface PlayersEndpoint {

    @GET("v1/players/{${Constantes.PATH_PLAYER_TAG}}")
    fun player(
            @Path(Constantes.PATH_PLAYER_TAG) playerTag: String
    ): Observable<PlayerDTO>

    @GET("v1/players/{${Constantes.PATH_PLAYER_TAG}}/upcomingchests")
    fun upcomingChests(
            @Path(Constantes.PATH_PLAYER_TAG) playerTag: String
    ): Observable<ListResponseDTO<ChestDTO>>

    @GET("v1/players/{${Constantes.PATH_PLAYER_TAG}}/battlelog")
    fun battleLog(
            @Path(Constantes.PATH_PLAYER_TAG) playerTag: String
    ): Observable<List<BattleLogDTO>>
}