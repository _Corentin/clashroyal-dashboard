package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 17/08/2018.
 */
class PlayerDTO: ResponseDTO() {

    @SerializedName("tag")
    val tag: String? = null

    @SerializedName("name")
    val name: String? = null

    @SerializedName("expLevel")
    val expLevel: Int? = null

    @SerializedName("trophies")
    val trophies: Int? = null

    @SerializedName("arena")
    val arena: ArenaDTO? = null

    @SerializedName("bestTrophies")
    val bestTrophies: Int? = null

    @SerializedName("wins")
    val wins: Int? = null

    @SerializedName("losses")
    val losses: Int? = null

    @SerializedName("battleCount")
    val battleCount: Int? = null

    @SerializedName("threeCrownWins")
    val threeCrownWins: Int? = null

    @SerializedName("challengeCardsWon")
    val challengeCardsWon: Int? = null

    @SerializedName("challengeMaxWins")
    val challengeMaxWins: Int? = null

    @SerializedName("tournamentCardsWon")
    val tournamentCardsWon: Int? = null

    @SerializedName("tournamentBattleCount")
    val tournamentBattleCount: Int? = null

    @SerializedName("role")
    val role: String? = null

    @SerializedName("donations")
    val donations: Int? = null

    @SerializedName("donationsReceived")
    val donationsReceived: Int? = null

    @SerializedName("totalDonations")
    val totalDonations: Int? = null

    @SerializedName("warDayWins")
    val warDayWins: Int? = null

    @SerializedName("clanCardsCollected")
    val clanCardsCollected: Int? = null

    @SerializedName("clan")
    val clan: ClanDTO? = null

    @SerializedName("leagueStatistics")
    val leagueStatistics: LeagueStatisticsDTO? = null

    @SerializedName("achievements")
    val achievements: List<AchievementDTO>? = null

    @SerializedName("cards")
    val cards: List<CardDTO>? = null

    @SerializedName("currentFavouriteCard")
    val currentFavouriteCard: CardDTO? = null

    @SerializedName("rank")
    val rank: Int? = 0

    @SerializedName("previousRank")
    val previousRank: Int? = 0
}