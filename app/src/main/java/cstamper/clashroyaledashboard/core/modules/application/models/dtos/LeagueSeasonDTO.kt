package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 17/08/2018.
 */
class LeagueSeasonDTO {

    @SerializedName("id")
    val id: String? = null

    @SerializedName("trophies")
    val trophies: Int? = null

    @SerializedName("bestTrophies")
    val bestTrophies: Int? = null
}