package cstamper.clashroyaledashboard.core.modules.application.datas.contracts

import cstamper.clashroyaledashboard.core.modules.application.models.dtos.CardDTO

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
interface ICardsManager {

    fun cards(callback: CardsCallback?)

    abstract class CardsCallback {
        fun onCardsRetrieved(cards: List<CardDTO>) {}
        abstract fun onError()
    }
}