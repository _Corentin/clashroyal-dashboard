package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
class WarDTO: ResponseDTO() {

    @SerializedName("tag")
    val tag: String? = null

    @SerializedName("name")
    val name: String? = null

    @SerializedName("seasonId")
    val seasonId: Int? = null

    @SerializedName("createdDate")
    val createdDate: String? = null

    @SerializedName("participants")
    val participants: List<ParticipantDTO>? = null

    @SerializedName("state")
    val state: String? = null

    @SerializedName("warEndTime")
    val warEndTime: String? = null

    @SerializedName("clan")
    val clan: ClanDTO? = null

    @SerializedName("rank")
    val rank: Int? = null

    @SerializedName("previousRank")
    val previousRank: Int? = null

    @SerializedName("location")
    val location: LocationDTO? = null

    @SerializedName("clanScore")
    val clanScore: Int? = null

    @SerializedName("badgeId")
    val badgeId: Int? = null

    @SerializedName("members")
    val members: Int? = null
}