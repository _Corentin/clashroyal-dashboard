package cstamper.clashroyaledashboard.core.modules.application.datas

import android.content.Context
import cstamper.clashroyaledashboard.core.modules.application.datas.contracts.ICardsManager
import cstamper.clashroyaledashboard.core.modules.application.datas.implementations.CardsManager
import cstamper.clashroyaledashboard.core.modules.retrofit.RetrofitModule

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
class DatasModule: RetrofitModule {

    private var cardsManager: ICardsManager

    constructor(context: Context, baseUrl: String): super(context, baseUrl) {
        // Todo: init all managers
        this.cardsManager = CardsManager(context, retrofit())
    }

    fun cardsManager(): ICardsManager =
            this.cardsManager
}