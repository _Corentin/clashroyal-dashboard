package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
class BattleLogDTO {

    @SerializedName("type")
    val type: String? = null

    @SerializedName("battleTime")
    val battleTime: String? = null

    @SerializedName("arena")
    val arena: ArenaDTO? = null

    @SerializedName("gameMode")
    val gameMode: GameModeDTO? = null

    @SerializedName("deckSelection")
    val deckSelection: String? = null

    @SerializedName("team")
    val team: List<PlayerDTO>? = null

    @SerializedName("opponent")
    val opponent: List<PlayerDTO>? = null
}