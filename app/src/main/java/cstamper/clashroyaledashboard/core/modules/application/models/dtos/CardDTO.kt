package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 17/08/2018.
 */
class CardDTO {

    @SerializedName("id")
    val id: Int? = null

    @SerializedName("name")
    val name: String? = null

    @SerializedName("level")
    val level: Int? = null

    @SerializedName("maxLevel")
    val maxLevel: Int? = null

    @SerializedName("count")
    val count: Int? = null

    @SerializedName("iconUrls")
    val iconUrls: IconDTO? = null

}