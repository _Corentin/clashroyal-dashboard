package cstamper.clashroyaledashboard.core.modules.application.datas.resources

import cstamper.clashroyaledashboard.core.modules.application.models.Constantes
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.ListResponseDTO
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.TournamentDTO
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
interface TournamentsEndpoint {

    @GET("v1/tournaments")
    fun tournaments(
        @Query(Constantes.QUERY_NAME)   name: String,
        @Query(Constantes.QUERY_LIMIT)  limit: Int,
        @Query(Constantes.QUERY_AFTER)  after: Int,
        @Query(Constantes.QUERY_BEFORE) before: Int
    ): Observable<ListResponseDTO<TournamentDTO>>

    @GET("v1/tournaments/{${Constantes.PATH_TOURNAMENT_TAG}}")
    fun tournament(
        @Path(Constantes.PATH_TOURNAMENT_TAG) tournamentTag: String
    ): Observable<TournamentDTO>
}