package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
class MemberDTO {

    @SerializedName("tag")
    val tag: String? = null

    @SerializedName("name")
    val name: String? = null

    @SerializedName("expLevel")
    val expLevel: Int? = null

    @SerializedName("trophies")
    val trophies: Int? = null

    @SerializedName("arena")
    val arena: ArenaDTO? = null

    @SerializedName("role")
    val role: String? = null

    @SerializedName("clanRank")
    val clanRank: String? = null

    @SerializedName("previousClanRank")
    val previousClanRank: Int? = null

    @SerializedName("donations")
    val donations: Int? = null

    @SerializedName("donationsReceived")
    val donationsReceived: Int? = null

    @SerializedName("clanChestPoints")
    val clanChestPoints: Int? = null
}