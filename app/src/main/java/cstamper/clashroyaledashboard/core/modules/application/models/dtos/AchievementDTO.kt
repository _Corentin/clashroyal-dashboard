package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 17/08/2018.
 */
class AchievementDTO {

    @SerializedName("name")
    val name: String? = null

    @SerializedName("starts")
    val stars: Int? = null

    @SerializedName("value")
    val value: Int? = null

    @SerializedName("target")
    val target: Int? = null

    @SerializedName("info")
    val info: String? = null

}