package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 17/08/2018.
 */
class ClanDTO: ResponseDTO() {

    @SerializedName("tag")
    val tag: String? = null

    @SerializedName("name")
    val name: String? = null

    @SerializedName("badgeId")
    val badgeId: Int? = null

    @SerializedName("type")
    val type: String? = null

    @SerializedName("clanScore")
    val clanStore: Int? = null

    @SerializedName("requiredTrophies")
    val requiredTrophies: Int? = null

    @SerializedName("donationsPerWeek")
    val donationsPerWeek: Int? = null

    @SerializedName("clanChestLevel")
    val clanChestLevel: Int? = null

    @SerializedName("clanChestMaxLevel")
    val clanChestMaxLevel: Int? = null

    @SerializedName("members")
    val members: Int? = null

    @SerializedName("location")
    val location: LocationDTO? = null

    @SerializedName("description")
    val description: String? = null

    @SerializedName("clanChestStatus")
    val clanChestStatus: String? = null

    @SerializedName("clanChestPoints")
    val clanChestPoints: Int? = null

    @SerializedName("memberList")
    val memberList: MemberDTO? = null

    @SerializedName("participants")
    val participants: Int? = null

    @SerializedName("battlesPlayed")
    val battlesPlayed: Int? = null

    @SerializedName("wins")
    val wins: Int? = null

    @SerializedName("crowns")
    val crowns: Int? = null

    @SerializedName("rank")
    val rank: Int? = null

    @SerializedName("previousRank")
    val previousRank: Int? = null
}