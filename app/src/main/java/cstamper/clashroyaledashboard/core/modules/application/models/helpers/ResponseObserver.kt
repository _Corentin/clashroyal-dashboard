package cstamper.clashroyaledashboard.core.modules.application.models.helpers

import io.reactivex.observers.DisposableObserver

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
abstract class ResponseObserver<T>: DisposableObserver<T>() {

    override fun onComplete() {}
}