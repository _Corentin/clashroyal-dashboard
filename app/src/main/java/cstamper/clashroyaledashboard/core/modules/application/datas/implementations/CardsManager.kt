package cstamper.clashroyaledashboard.core.modules.application.datas.implementations

import android.content.Context
import android.util.Log
import cstamper.clashroyaledashboard.core.modules.application.datas.contracts.ICardsManager
import cstamper.clashroyaledashboard.core.modules.application.datas.resources.CardsEndpoint
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.CardDTO
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.ListResponseDTO
import cstamper.clashroyaledashboard.core.modules.application.models.helpers.ResponseObserver
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
class CardsManager: ICardsManager {

    private val endpoint: CardsEndpoint
    private val context: Context

    constructor(context: Context, retrofit: Retrofit) {
        this.context = context
        this.endpoint = retrofit.create(CardsEndpoint::class.java)
    }

    override fun cards(callback: ICardsManager.CardsCallback?) {
        this.endpoint.cards()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(object: ResponseObserver<ListResponseDTO<CardDTO>>() {
                    override fun onNext(t: ListResponseDTO<CardDTO>) {
                        if (t.items != null)
                            callback?.onCardsRetrieved(t.items!!)
                        else
                            callback?.onError()
                    }

                    override fun onError(e: Throwable) {
                        callback?.onError()
                    }
                })
    }
}