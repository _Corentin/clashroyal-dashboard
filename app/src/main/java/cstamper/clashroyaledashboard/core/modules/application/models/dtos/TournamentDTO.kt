package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
class TournamentDTO: ResponseDTO() {

    @SerializedName("tag")
    val tag: String? = null

    @SerializedName("type")
    val type: String? = null

    @SerializedName("status")
    val status: String? = null

    @SerializedName("creatorTag")
    val creatorTag: String? = null

    @SerializedName("name")
    val name: String? = null

    @SerializedName("description")
    val description: String? = null

    @SerializedName("capacity")
    val capacity: Int? = null

    @SerializedName("maxCapacity")
    val maxCapacity: Int? = null

    @SerializedName("preparationDuration")
    val preparationDuration: Int? = null

    @SerializedName("duration")
    val duration: Int? = null

    @SerializedName("createdTime")
    val createdTime: String? = null
}