package cstamper.clashroyaledashboard.core.modules.application.models.dtos

import com.google.gson.annotations.SerializedName

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
class ParticipantDTO {

    @SerializedName("tag")
    val tag: String? = null

    @SerializedName("name")
    val name: String? = null

    @SerializedName("cardsEarned")
    val cardsEarned: Int? = null

    @SerializedName("battlesPlayed")
    val battlesPlayer: Int? = null

    @SerializedName("wins")
    val wins: Int? = null
}