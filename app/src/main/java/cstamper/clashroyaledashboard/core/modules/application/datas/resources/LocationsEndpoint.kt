package cstamper.clashroyaledashboard.core.modules.application.datas.resources

import cstamper.clashroyaledashboard.core.modules.application.models.Constantes
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.*
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
interface LocationsEndpoint {

    @GET("v1/locations")
    fun locations(
            @Query(Constantes.QUERY_LIMIT) limit: Int,
            @Query(Constantes.QUERY_AFTER) after: Int,
            @Query(Constantes.QUERY_BEFORE) before: Int
    ): Observable<ListResponseDTO<LocationDTO>>

    @GET("v1/locations/{${Constantes.PATH_LOCATION_ID}}")
    fun location(
            @Path(Constantes.PATH_LOCATION_ID) locationId: String
    ): Observable<LocationDTO>

    @GET("v1/locations/{${Constantes.PATH_LOCATION_ID}}/rankings/clans")
    fun rankingClans(
            @Path(Constantes.PATH_LOCATION_ID) locationId: String,
            @Query(Constantes.QUERY_LIMIT) limit: Int,
            @Query(Constantes.QUERY_AFTER) after: Int,
            @Query(Constantes.QUERY_BEFORE) before: Int
    ): Observable<ListResponseDTO<ClanDTO>>

    @GET("v1/locations/{${Constantes.PATH_LOCATION_ID}}/rankings/players")
    fun rankingPlayers(
            @Path(Constantes.PATH_LOCATION_ID) locationId: String,
            @Query(Constantes.QUERY_LIMIT) limit: Int,
            @Query(Constantes.QUERY_AFTER) after: Int,
            @Query(Constantes.QUERY_BEFORE) before: Int
    ): Observable<ListResponseDTO<PlayerDTO>>

    @GET("v1/locations/{${Constantes.PATH_LOCATION_ID}}/rankings/clanwars")
    fun rankingClanWars(
            @Path(Constantes.PATH_LOCATION_ID) locationId: String,
            @Query(Constantes.QUERY_LIMIT) limit: Int,
            @Query(Constantes.QUERY_AFTER) after: Int,
            @Query(Constantes.QUERY_BEFORE) before: Int
    ): Observable<ListResponseDTO<WarDTO>>
}