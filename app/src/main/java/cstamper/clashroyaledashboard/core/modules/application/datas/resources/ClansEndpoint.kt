package cstamper.clashroyaledashboard.core.modules.application.datas.resources

import cstamper.clashroyaledashboard.core.modules.application.models.Constantes
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.ClanDTO
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.ListResponseDTO
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.MemberDTO
import cstamper.clashroyaledashboard.core.modules.application.models.dtos.WarDTO
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Corentin Stamper
 * @company cstamper
 * @email stamper.c@laposte.net
 * @date 28/08/2018.
 */
interface ClansEndpoint {

    @GET("v1/clans")
    fun clans(
            @Query(Constantes.QUERY_NAME) name: String,
            @Query(Constantes.QUERY_LOCATION_ID) locationId: Int,
            @Query(Constantes.QUERY_MIN_MEMBERS) minMembers: Int,
            @Query(Constantes.QUERY_MAX_MEMBERS) maxMembers: Int,
            @Query(Constantes.QUERY_MIN_SCORE)   minScore: Int,
            @Query(Constantes.QUERY_LIMIT)       limit: Int,
            @Query(Constantes.QUERY_AFTER)       after: Int,
            @Query(Constantes.QUERY_BEFORE)      before: Int
    ): Observable<ListResponseDTO<ClanDTO>>

    @GET("v1/clans/{${Constantes.PATH_CLAN_TAG}}")
    fun clan(
            @Path(Constantes.PATH_CLAN_TAG) clanTag: String
    ): Observable<ClanDTO>

    @GET("v1/clans/{${Constantes.PATH_CLAN_TAG}}/members")
    fun members(
            @Path(Constantes.PATH_CLAN_TAG) clanTag: String,
            @Query(Constantes.QUERY_LIMIT)  limit: Int,
            @Query(Constantes.QUERY_AFTER)  after: Int,
            @Query(Constantes.QUERY_BEFORE) before: Int
    ): Observable<ListResponseDTO<MemberDTO>>

    @GET("v1/clans/{${Constantes.PATH_CLAN_TAG}}/warlog")
    fun warlog(
            @Path(Constantes.PATH_CLAN_TAG) clanTag: String,
            @Query(Constantes.QUERY_LIMIT)  limit: Int,
            @Query(Constantes.QUERY_AFTER)  after: Int,
            @Query(Constantes.QUERY_BEFORE) before: Int
    ): Observable<ListResponseDTO<WarDTO>>

    @GET("v1/clans/{${Constantes.PATH_CLAN_TAG}}/currentwar")
    fun currentwar(
            @Path(Constantes.PATH_CLAN_TAG) clanTag: String,
            @Query(Constantes.QUERY_LIMIT)  limit: Int,
            @Query(Constantes.QUERY_AFTER)  after: Int,
            @Query(Constantes.QUERY_BEFORE) before: Int
    ): Observable<WarDTO>
}